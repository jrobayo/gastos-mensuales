package com.example.controlgastos;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.button.MaterialButton;

public class PasswordR extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_password_r);

        getSupportActionBar().hide();

        TextView email = (TextView) findViewById(R.id.remail);

        MaterialButton recontraseña = (MaterialButton) findViewById(R.id.recontraseña);

        recontraseña.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (email.getText().toString().equals("edgardeve@hotmail.com")) {
                    Toast.makeText(PasswordR.this, "Escribe el código enviado a tu correo", Toast.LENGTH_SHORT).show();

                } else
                    Toast.makeText(PasswordR.this, "Usuario no registrado", Toast.LENGTH_SHORT).show();

            }
        });
    }
}