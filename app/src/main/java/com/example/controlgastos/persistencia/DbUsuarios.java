package com.example.controlgastos.persistencia;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import androidx.annotation.Nullable;

public class DbUsuarios extends DbHelper {

    Context context;

    /* Crud
    * C = CREAR
    * R = LEER
    * U = UTILIZAR
    * D = ELIMINAR */

    // constructor

    public DbUsuarios(@Nullable Context context){
        // SUPER: Llama al contructor de la clase padre
        super(context);
        this.context = context;
    }

    public Boolean insertarUsuario(String nombre, String apellido, String usuario, String correo ,String contrasena){
        DbHelper dbHelper = new DbHelper(context);// Intancia del objeto DbHleper = nuestra base de datos
        SQLiteDatabase db = dbHelper.getWritableDatabase();//agregamos datos

        ContentValues values = new ContentValues();//instancia de nuestro objeto values
        values.put("nombre", nombre);
        values.put("apellido", apellido);
        values.put("usuario", usuario);
        values.put("correo", correo);
        values.put("contrasena", contrasena);

        long result = db.insert(TABLE_USERS, null, values);
        if (result==-1)
            return false;
        else
            return true;
    }

    public Boolean checknomusuario(String nomusuario){
        DbHelper dbHelper = new DbHelper(context);//instancia del objeto dbhelper
        SQLiteDatabase db= dbHelper.getWritableDatabase();//coleccion de filas que son aleatorias
        Cursor cursor = db.rawQuery("SELECT * FROM usuarios WHERE usuario =?" , new String[] {nomusuario});
        if (cursor.getCount()>0)
            return true;
        else
            return false;
    }

    public Boolean checkcontraseña(String nomusuario, String contraseña){
        DbHelper dbHelper = new DbHelper(context);//instancia del objeto dbhelper
        SQLiteDatabase db= dbHelper.getWritableDatabase();
        //coleccion de filas que son aleatorias
        Cursor cursor = db.rawQuery("SELECT * FROM usuarios WHERE usuario =? and contrasena=?" , new String[] {nomusuario, contraseña});
        if (cursor.getCount()>0)
            return true;
        else
            return false;
    }


}
