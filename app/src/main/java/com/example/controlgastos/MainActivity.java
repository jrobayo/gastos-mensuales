package com.example.controlgastos;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.controlgastos.persistencia.DbUsuarios;
import com.google.android.material.button.MaterialButton;

public class MainActivity extends AppCompatActivity {

    EditText email, password;
    Button loginbtn, btnRegis;
    DbUsuarios DB;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        getSupportActionBar().hide();

        email = findViewById(R.id.email);
        password = findViewById(R.id.password);
        MaterialButton loginbtn = (MaterialButton) findViewById(R.id.loginboton);
        MaterialButton btnRegis = (MaterialButton) findViewById(R.id.registro);
        DB = new DbUsuarios(this);


        loginbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String user = email.getText().toString();
                String pass = password.getText().toString();

                if (TextUtils.isEmpty(user) || TextUtils.isEmpty(pass))
                    Toast.makeText(MainActivity.this, "Todos los campos deben llenarse", Toast.LENGTH_SHORT).show();
                else {
                    Boolean checkuserpass = DB.checkcontraseña(user, pass);
                    if (checkuserpass == true) {
                        Intent intent = new Intent(getApplicationContext(), Productos.class);
                        startActivity(intent);
                        limpiar();
                    }else {
                        Toast.makeText(MainActivity.this, "login incorrecto", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });
        btnRegis.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), Registro.class);
                startActivity(intent);
            }
        });
    }

    public void regcontraseña(View p) {
        Intent nvista = new Intent(this, PasswordR.class);
        startActivity(nvista);

    }

    private void limpiar() {

        email.setText("");
        password.setText("");
    }
}
