package com.example.controlgastos;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Dialog;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.controlgastos.persistencia.DbHelper;
import com.example.controlgastos.persistencia.DbUsuarios;
import com.google.android.material.button.MaterialButton;

public class Registro extends AppCompatActivity {

    EditText nombre1, apellido1, username1, correo, password1, repassword;
    Button btnRegistro, volversin;
    DbUsuarios DB;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registro);

        getSupportActionBar().hide();

        nombre1 = findViewById(R.id.nombre);
        apellido1 = findViewById(R.id.apellidos);
        username1 = findViewById(R.id.username);
        correo = findViewById(R.id.email);
        password1 = findViewById(R.id.password);
        repassword = findViewById(R.id.repassword);
        btnRegistro = findViewById(R.id.btnRegistro);
        volversin = findViewById(R.id.volversining);
        DB = new DbUsuarios(this);

        btnRegistro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String nomb = nombre1.getText().toString();
                String apel = apellido1.getText().toString();
                String user = username1.getText().toString();
                String email = correo.getText().toString();
                String pass = password1.getText().toString();
                String repass = repassword.getText().toString();

                if (TextUtils.isEmpty(nomb) || TextUtils.isEmpty(apel) || TextUtils.isEmpty(user) || TextUtils.isEmpty(email) || TextUtils.isEmpty(pass) || TextUtils.isEmpty(repass))
                    Toast.makeText(Registro.this, "Se requiere llenar todos los campos", Toast.LENGTH_SHORT).show();
                else {
                    if (pass.equals(repass)) {
                        Boolean checkuser = DB.checknomusuario(user);
                        if (checkuser == false) {
                            boolean insert = DB.insertarUsuario(nomb, apel, user, email, pass);
                            if (insert == true) {
                                Toast.makeText(Registro.this, "Registro agregado correctamente", Toast.LENGTH_LONG).show();
                                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                                startActivity(intent);
                            } else {
                                Toast.makeText(Registro.this, "Registro fallido", Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            Toast.makeText(Registro.this, "El usuario ya existe", Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(Registro.this, "La contraseña no coincide", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });
        volversin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                startActivity(intent);
            }
        });
    }
}
        //
        //btnRegistro = findViewById(R.id.btnRegistro);
        //ShowDialog = findViewById(R.id.btnRegistro);

        //Button btnRegistro = (Button) findViewById(R.id.btnRegistro);
        //Dialog dialog; // Nuevo

        // conectamos la clase .java
        //dialog = new Dialog(Registro.this); // Indicamos donde se mostrará el el Dialog
        // conectamos al .xml
        //dialog.setContentView(R.layout.custom_dialog); // Conexión al archivo custom_dialog.xml
        //if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP){
        //    dialog.getWindow().setBackgroundDrawable(getDrawable(R.drawable.background2));

        //}

        //dialog.getWindow().setBackgroundDrawable(getDrawable(R.drawable.background2));// Asiganamos el Fondo del Dialog (Background)
        //dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT,ViewGroup.LayoutParams.WRAP_CONTENT);
        //dialog.setCancelable(false);

        // Declaramos los botones
        //Button Okey = dialog.findViewById(R.id.btn_okay);
        //Button Cancel = dialog.findViewById(R.id.btn_cancel);

        //Okey.setOnClickListener(new View.OnClickListener() {
        //    @Override
        //    public void onClick(View v) {

        //        Toast.makeText(Registro.this, "Okay", Toast.LENGTH_SHORT).show();
        //        dialog.dismiss();
        //    }
        //});

        //Cancel.setOnClickListener(new View.OnClickListener() {
        //    @Override
        //    public void onClick(View v) {

        //        Toast.makeText(Registro.this, "Cancel", Toast.LENGTH_SHORT).show();
        //        dialog.dismiss();
        //    }
        //});


        //ShowDialog.setOnClickListener(new View.OnClickListener() {
        //    @Override
        //    public void onClick(View v) {

        //        dialog.show();
        //    }
        //});

        /*

                DbHelper dbHelper = new DbHelper(Registro.this); // Instancia de un objeto
                SQLiteDatabase db = dbHelper.getWritableDatabase(); //Crearla = Escribir
                // Validación
                if(db != null){
                    Toast.makeText(Registro.this, "USUARIO CREADO CON EXITO",Toast.LENGTH_LONG).show();
                    finish();

                }else {
                    Toast.makeText(Registro.this,"ERROR AL CREAR USUARIO", Toast.LENGTH_LONG).show();
                }

            }
        });*/
