package com.example.controlgastos;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

import com.google.android.material.button.MaterialButton;

import java.util.ArrayList;
import java.util.List;

public class Productos extends AppCompatActivity {

    List<ListElement> elements;
    Button ingButton;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_productos);

        init();
        //getSupportActionBar().hide();
        MaterialButton ingButton = (MaterialButton) findViewById(R.id.ingButton);

        /*ingButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View f) {
                Intent intent = new Intent(getApplicationContext(), CardItems.class);
                startActivity(intent);

            }

        });*/

    }

    public void Cards(View p) {
        Intent nvista = new Intent(this, CardsItems.class);
        startActivity(nvista);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menudeopciones,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        int opt = item.getItemId();
        float valor;

        switch (opt){
            case R.id.ajustes:
                finish();
                return true;
            case R.id.cambiar_cuenta:
                finish();
                return true;
            case R.id.salir:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }

    }
    public void init(){
        elements = new ArrayList<ListElement>(); // Instancia del objeto elements
        elements.add(new ListElement("#775447", "Ingresos", "Registro de ingresos", "IR")); // Tarjeta 1
        elements.add(new ListElement("#607d8b", "Gastos", "Registro de gastos", "IR")); // Tarjeta 2
        elements.add(new ListElement("#03a9f4", "Logros", "Seguimiento de sueños", "IR")); // Tarjeta 3



        // Declaramos el ListAdapter y recibe una lista y el context = de donde viene
        ListAdapter listAdapter = new ListAdapter(elements, this);
        // Declaramos el RecyclerView
        RecyclerView recyclerView = findViewById(R.id.listRecyclerView);
        // Movemos unos parametros en verdadero
        recyclerView.setHasFixedSize(true);
        // Listado lineal setLayoutManager
        recyclerView.setLayoutManager(new LinearLayoutManager(this,LinearLayoutManager.VERTICAL,false));

        recyclerView.setAdapter(listAdapter);

    }


}